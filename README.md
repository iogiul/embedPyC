## Aim

This repo contains a simple test of embedding Python code in C++. 
In particular, it wraps in C++ a simple Python function adding two double numbers
The function is add_two_numbers in the module testlib.py
The C++ code is in main.cpp 
The official Python documentation for embedding can be found at [https://docs.python.org/3/extending/embedding.html](https://docs.python.org/3/extending/embedding.html)

## COmpiling and running the test 

### Requirements
A working c++ compiler and a working Python installation

### How to set the code
Just change the variable modulepath in the main function setting the proper absolute path to the testlib.py location

### How to compile 
The code can be compiled as an usual C++ code, but the Python headers and  libraries should be linked properly.
There are two main ways: 

#### Cmake
This is the easiest way since cmake sets automatically all the right  dependencies  (if it can find a working Python installation)

- Create a build folder, `cmake build`
- Go inside the folder and run cmake, `cd build; cmake .`.
- Run make  `make `
- This will create the executable embedPyC ` ./embedPyC`

#### Manual compilation
In order to compile the C++ code we have to retrieve the CFLAGS and libraries setting for the Python library. 
In order to retrieve it is possible to use the python-config commands using pythonX.Y-config --cflags and pythonX.Y-config --ldflags --embed, where X.Y is the python 
version you are using (python3-config should be fine if you are using python3). 

- export the cflags `export CPFLAGS=$(pythonX.Y-config --cflags)`
- export the lflags `export LCPFLAGS=$(pythonX.Y-config --ldflags --embed)`
- compile `g++ main.cpp -o embedPyC $CPFLAGS $LCPFLAGS`


