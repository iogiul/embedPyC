#include <iostream>
#include <Python.h>
#include <iostream>


/**
 * Method to append the path of the module to load to the sys.path in Python
 * @param path  string containing the absolute path of the module to load
 * @return
 */
int set_path(std::string path){

    auto modulo = PyImport_ImportModule("sys");
    auto pathlist = PyObject_GetAttrString(modulo,"path");
    auto path_to_insert = PyUnicode_FromString((char*)path.c_str());
    PyList_Insert(pathlist, 0,path_to_insert);
    return EXIT_SUCCESS;
}

/**
 * Get the function to be called
 * @param modulename name of the python file (without the .py) containing the function to get
 * @param funcname name of the function to get
 * @return the pointer to the Python function
 */
PyObject * get_function(std::string modulename, std::string funcname){
   auto  myModule = PyImport_ImportModule("testlib");
   auto  myFunction = PyObject_GetAttrString(myModule,"add_two_numbers");

   return myFunction;
}

/**
 * Wrapper to call the Python function with the signature func(a,b) where a and b are two number
 * @param func Pointer to the Python function
 * @param a double value a
 * @param b  double value b
 * @return the result of the python function as a double
 */
double callPyfunction(PyObject * func, double a, double b){

    auto  args = PyTuple_Pack(2,PyFloat_FromDouble(a),PyFloat_FromDouble(b));
    auto  myResult = PyObject_CallObject(func, args);

    return PyFloat_AsDouble(myResult);

}


int main(int argc, char *argv[]) {


    std::string modulepath="/Users/giulianoiorio/CLionProjects/import_from_python";
    std::string modulename="test";
    std::string funcname="add_two_numbers";
    double a=8.0, b=5.0;


    Py_Initialize();
    set_path(modulepath);

    auto myfunction = get_function("testlib","add_two_numbers");
    auto result = callPyfunction(myfunction,a,b);

    Py_Finalize();

    std::cout<<"Inputs "<<a<<" "<<b<<std::endl;
    std::cout<<"Result from Py function "<<result<<std::endl;



    return EXIT_SUCCESS;


    /*Deal with Lists
    myModule = PyImport_ImportModule("testlib");
    auto myList = PyObject_GetAttrString(myModule,"pippo");
    auto puppo=PyBytes_FromString((char*)modulepath.c_str());
    PyList_Append(myList, puppo);
    auto item=PyList_GetItem(myList,0);
    std::cout<<PyLong_AsLong(item)<<std::endl;
    item=PyList_GetItem(myList,1);
    std::cout<<PyBytes_AsString(item)<<std::endl;
    item=PyList_GetItem(myList,2);
    std::cout<<PyLong_AsLong(item)<<std::endl;
    */

    //const char *bytes = PyBytes_AS_STRING(myModuleString);
    //std::cout<<*bytes<<std::endl;


    return 0;
}
